import request from '@/utils/request';
import api from '@/utils/api'

export type LoginParamsType = {
    client_id: string,
    client_secret: string,
    scope: string,
    grant_type: string,
    username: string,
    password: string,
    captchaComp: { captchaKey: string, captchaCode: string },
    captcha_key: string,
    captcha_code: string,
    mobile?: string;
    captcha?: string;
};


export async function getFakeCaptcha(mobile: string) {
    return request(`/api/login/captcha?mobile=${mobile}`);
}

export async function accountLogin(loginParams: LoginParamsType) {
    return request(api.accountLogin, {
        method: 'POST',
        params: loginParams
    });
}
