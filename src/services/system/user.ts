import request from '@/utils/request';
import api from "@/utils/api";


/**
 * 查询当前用户
 */
export async function currentUser() {
  return request(api.currentUser);
}

export async function queryCurrent(): Promise<any> {
  return request('/api/currentUser');
}

export async function queryNotices(): Promise<any> {
  return request('/api/notices');
}
