import request from '@/utils/request';
import api from '@/utils/api';

/**
 * 获取路由
 */
export async function listRouteData(): Promise<any> {
  return request(api.listRoute, {
    method: 'GET',
  });
}
