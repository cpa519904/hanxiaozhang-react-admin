import {Effect, Reducer} from 'umi';
import {CurrentUser, GeographicItemType} from './data.d';
import {currentUser, queryCity, queryProvince} from './service';
import stringUtil from "@/utils/stringUtil";
import moment from "moment";
import cityJson from './geographic/city.json';
import provinceJson from './geographic/province.json';

export interface ModalState {
  currentUser?: Partial<CurrentUser>;
  province?: GeographicItemType[];
  city?: GeographicItemType[];
  isLoading?: boolean;
}

export interface ModelType {
  namespace: string;
  state: ModalState;
  effects: {
    fetchCurrent: Effect;
    fetchProvince: Effect;
    fetchCity: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer<ModalState>;
    changeNotifyCount: Reducer<ModalState>;
    setProvince: Reducer<ModalState>;
    setCity: Reducer<ModalState>;
    changeLoading: Reducer<ModalState>;
  };
}


const setGeographic = (provinceItem: any, cityItem: any) => {
  return {
    province: {
      label: provinceItem.name,
      key: provinceItem.id,
    },
    city: {
      label: cityItem.name,
      key: cityItem.id,
    }
  }
}

const Model: ModelType = {
  namespace: 'accountAndsettings',

  state: {
    currentUser: {},
    province: [],
    city: [],
    isLoading: false,
  },

  effects: {
    // 当前用户
    * fetchCurrent({callback }, {call, put}) {
      const response = yield call(currentUser);
      const {code, data} = response;
      if (code === 1) {
        if (stringUtil.isNotEmpty(data.birth)) {
          data.birth = moment(data.birth, 'YYYY-MM-DD')
        }
        if (data.province > 0 && data.city > 0) {
          const province = provinceJson.filter((item: any) => item.id === data.province.toString());
          const city = cityJson[data.province].filter((item: any) => item.id === data.city.toString());
          data.geographic = setGeographic(province[0], city[0]);
        }
      }
      yield put({
        type: 'saveCurrentUser',
        payload: data,
      });
      if (callback) {
        callback(data);
      }
    },
    // 省
    * fetchProvince(_, {call, put}) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryProvince);
      yield put({
        type: 'setProvince',
        payload: response.data,
      });
    },
    // 城市
    * fetchCity({payload}, {call, put}) {
      const response = yield call(queryCity, payload);
      yield put({
        type: 'setCity',
        payload: response.data,
      });
    },
  },

  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(state = {}, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
    setProvince(state, action) {
      return {
        ...state,
        province: action.payload,
      };
    },
    setCity(state, action) {
      return {
        ...state,
        city: action.payload,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        isLoading: action.payload,
      };
    },
  },
};

export default Model;
