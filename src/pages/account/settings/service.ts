import request from '@/utils/request';
import api from "@/utils/api";
import {CurrentUser} from "./data.d";


/**
 * 更新用户
 *
 * @param params
 */
export async function updateUser(params: CurrentUser) {
  return request(api.userUpdateUser, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}

/**
 * 查询当前用户
 */
export async function currentUser() {
  return request(api.currentUser);
}

/**
 * 查询省，直接用mock
 */
export async function queryProvince() {
  return request('/api/geographic/province');
}

/**
 * 查询市，直接用mock
 * @param province
 */
export async function queryCity(province: string) {
  return request(`/api/geographic/city/${province}`);
}


/**
 * 重置密码
 *
 * @param params
 */
export async function updatePassword(params: any) {
  return request(api.userUpdatePassword, {
    method: 'POST',
    data: {
      ...params
    },
  });
}
