export interface TagType {
  key: string;
  label: string;
}

export interface GeographicItemType {
  name: string;
  id: string;
}

export interface GeographicType {
  province: GeographicItemType;
  city: GeographicItemType;
}

export interface NoticeType {
  id: string;
  title: string;
  logo: string;
  description: string;
  updatedAt: string;
  member: string;
  href: string;
  memberLink: string;
}

export interface CurrentUser {
  userId: number; // 用户主键 userid
  username: string; // 用户名
  name: string; // 名称
  nickName: string; // 昵称
  sex: number; // 性别
  birth: string; // 出身日期
  phone: string; // 手机号码
  email: string; // 邮箱
  country: number; // 国家
  geographic: GeographicType;
  province: number; // 省
  city: number; // 市
  county: number; // 县
  address: string; // 地址
  headHeadPath: string; // 头像地址  avatar
  introduction: string; // 个人简介  profile
  signature: string; // 个人签名
  notice: NoticeType[];
  title: string;
  group: string;
  tags: TagType[];
  notifyCount: number;
  unreadCount: number;



}
