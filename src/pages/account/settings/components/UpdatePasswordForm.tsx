import React from 'react';
import {Form, Input, Modal, Row, Col, notification} from 'antd';
import {updatePassword} from './../service'
import {clearTokens} from "@/utils/token";
import {history} from "@@/core/history";


export interface UpdateFormProps {
  updatePasswordModalVisible: boolean;
  handleUpdatePasswordModalVisible: any;
}


const UpdatePasswordForm: React.FC<UpdateFormProps> = (props) => {

  const {updatePasswordModalVisible, handleUpdatePasswordModalVisible} = props;


  const [form] = Form.useForm();


  const onCancel = () => {
    form.resetFields();
    handleUpdatePasswordModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: any) => {
      const data = await updatePassword(values);
      if (data.code === 1) {
        onCancel();
        notification.warn({
          message: '修改密码后，请重新登陆!',
        });
        clearTokens();
        history.push('/user/login');
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改密码"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updatePasswordModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="resetPasswordForm" form={form}
            layout="vertical"
      >

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='原密码'
              name="oldPassword"
              rules={[{required: true, message: '请输入原密码'}]}
            >
              <Input type={"password"} placeholder="请输入原密码"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='新密码'
              name="passwordOne"
              rules={[{required: true, message: '请输入新密码'}]}
            >
              <Input type={"password"} placeholder="请输入新密码"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='确认新密码'
              name="passwordTwo"
              rules={[{required: true, message: '请输入新密码'}]}
            >
              <Input type={"password"} placeholder="请输入新密码"/>
            </Form.Item>
          </Col>
        </Row>

      </Form>

    </Modal>
  );
};

export default UpdatePasswordForm;
