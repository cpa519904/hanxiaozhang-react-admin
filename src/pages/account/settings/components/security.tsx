import {FormattedMessage, formatMessage} from 'umi';
import {Component} from 'react';
import {List} from 'antd';
import UpdatePasswordForm from './UpdatePasswordForm'


type Unpacked<T> = T extends (infer U)[] ? U : T;

const passwordStrength = {
  strong: (
    <span className="strong">
      <FormattedMessage id="accountandsettings.security.strong" defaultMessage="Strong"/>
    </span>
  ),
  medium: (
    <span className="medium">
      <FormattedMessage id="accountandsettings.security.medium" defaultMessage="Medium"/>
    </span>
  ),
  weak: (
    <span className="weak">
      <FormattedMessage id="accountandsettings.security.weak" defaultMessage="Weak"/>
      Weak
    </span>
  ),
};

interface SecurityProps {
  updatePasswordModalVisible: boolean;
}


class SecurityView extends Component<any, SecurityProps> {

  constructor(props: any) {
    super(props);
    this.state = {
      updatePasswordModalVisible: false,
    };
  }

  getData = () => [
    {
      title: formatMessage({id: 'accountandsettings.security.password'}, {}),
      description: (
        <>
          {formatMessage({id: 'accountandsettings.security.password-description'})}：
          {passwordStrength.strong}
        </>
      ),
      actions: [
        <a key="Modify" onClick={() => {
          this.handleUpdatePasswordModalVisible(true)
        }}>
          <FormattedMessage id="accountandsettings.security.modify" defaultMessage="Modify"/>
        </a>,
      ],
    },
    {
      title: formatMessage({id: 'accountandsettings.security.phone'}, {}),
      description: `${formatMessage(
        {id: 'accountandsettings.security.phone-description'},
        {},
      )}：138****8293`,
      actions: [
        <a key="Modify">
          <FormattedMessage id="accountandsettings.security.modify" defaultMessage="Modify"/>
        </a>,
      ],
    },
    {
      title: formatMessage({id: 'accountandsettings.security.question'}, {}),
      description: formatMessage({id: 'accountandsettings.security.question-description'}, {}),
      actions: [
        <a key="Set">
          <FormattedMessage id="accountandsettings.security.set" defaultMessage="Set"/>
        </a>,
      ],
    },
    {
      title: formatMessage({id: 'accountandsettings.security.email'}, {}),
      description: `${formatMessage(
        {id: 'accountandsettings.security.email-description'},
        {},
      )}：ant***sign.com`,
      actions: [
        <a key="Modify">
          <FormattedMessage id="accountandsettings.security.modify" defaultMessage="Modify"/>
        </a>,
      ],
    },
    {
      title: formatMessage({id: 'accountandsettings.security.mfa'}, {}),
      description: formatMessage({id: 'accountandsettings.security.mfa-description'}, {}),
      actions: [
        <a key="bind">
          <FormattedMessage id="accountandsettings.security.bind" defaultMessage="Bind"/>
        </a>,
      ],
    },
  ];


  handleUpdatePasswordModalVisible = (flag: boolean) => {
    this.setState({
      updatePasswordModalVisible: flag
    })
  }

  render() {
    const {updatePasswordModalVisible} = this.state;
    const data = this.getData();
    return (
      <>
        <List<Unpacked<typeof data>>
          itemLayout="horizontal"
          dataSource={data}
          renderItem={(item) => (
            <List.Item actions={item.actions}>
              <List.Item.Meta title={item.title} description={item.description}/>
            </List.Item>
          )}
        />

        {/* 更新密码表单 */}
        <UpdatePasswordForm
          updatePasswordModalVisible={updatePasswordModalVisible}
          handleUpdatePasswordModalVisible={this.handleUpdatePasswordModalVisible}
        ></UpdatePasswordForm>
      </>
    );
  }
}

export default SecurityView;
