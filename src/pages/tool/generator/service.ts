import request from '@/utils/request';
import {TableListParams} from './data.d';
import api from "@/utils/api";
import {TableListItem} from "@/pages/base/dict/data";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listGenerator, {
    params,
  });
}


/**
 * 获取配置
 *
 * @param params
 */
export async function getConfig(): Promise<any> {
  return request(api.getConfigGenerator);
}



/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateGenerator, {
    method: 'POST',
    data: {
      ...params
    },
  });
}

/**
 * 生成
 *
 * @param tableName
 */
export async function code(tableName: string) {
  return request(api.codeGenerator, {
    method: 'POST',
    data: {
      'tableName': tableName,
    },
    requestType: 'form',
  });
}


/**
 * 批量生成
 *
 * @param tableNames
 */
export async function batchCode(tableNames: string[]) {
  return request(api.batchCodeGenerator, {
    method: 'POST',
    data: {
      'tableNames': tableNames,
    },
    requestType: 'form',
  });
}

