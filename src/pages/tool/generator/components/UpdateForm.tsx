import React, {useEffect} from 'react';
import {Form, Input, Modal, Radio, Row, Col} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改配置"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='作者姓名'
              name="author"
              rules={[{required: true, message: '请输入作者姓名'}]}
            >
              <Input placeholder="请输入作者姓名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='邮箱'
              name="email"
              rules={[{required: true, message: '请输入邮箱'}]}
            >
              <Input placeholder="请输入邮箱"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='版本'
              name="since"
              rules={[{required: true, message: '请输入版本'}]}
            >
              <Input placeholder="请输入版本"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='包路径'
              name="packagePath"
              rules={[{required: true, message: '请输入包路径'}]}
            >
              <Input placeholder="请输入包路径"/>
            </Form.Item>
          </Col>
        </Row>


        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='表前缀'
              name="tablePrefix"
              rules={[{required: true, message: '请输入表前缀'}]}
            >
              <Input placeholder="请输入表前缀"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='去除表前缀'
              name="removeTablePrefix"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={true}>是</Radio>
                <Radio value={false}>否</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </Modal>
  );
};

export default UpdateForm;
