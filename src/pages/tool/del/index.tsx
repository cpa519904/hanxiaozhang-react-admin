import { Button, Result } from 'antd';
import React from 'react';
import { history } from 'umi';

const GenerateCodePage: React.FC = () => (
    <Result
        status="404"
        title="开发中"
        subTitle="页面及功能正在开发中 ... ..."
        extra={
            <Button type="primary" onClick={() => history.push('/')}>
                返回首页
      </Button>
        }
    />
);

export default GenerateCodePage;
