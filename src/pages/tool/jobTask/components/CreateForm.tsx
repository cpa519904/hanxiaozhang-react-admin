import React from 'react';
import {Modal, Form, Row, Col, Input, Radio} from 'antd';
import {TableListItem} from "../data.d";

interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  handleSave: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible, handleSave, actionRef} = props;

  const [form] = Form.useForm();


  const onCancel = () => {
    form.resetFields();
    handleModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      const success = await handleSave(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      destroyOnClose
      title="新建任务"
      visible={modalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >
      <Form className="addForm"
            initialValues={{jobStatus: 0}}
            form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='任务名称'
              name="jobName"
              rules={[{required: true, message: '请输入任务名称'}]}
            >
              <Input placeholder="请输入任务名称"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='任务分组'
              name="jobGroup"
              rules={[{required: true, message: '请输入任务分组'}]}
            >
              <Input placeholder="请输入任务分组"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='全限定名'
              name="beanClass"
              rules={[{required: true, message: '请输入全限定名'}]}
            >
              <Input placeholder="请输入全限定名"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='cron表达式'
              name="cronExpression"
              rules={[{required: true, message: '请输入cron表达式'}]}
            >
              <Input placeholder="请输入cron表达式"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='任务参数'
              name="jobParams"
            >
              <Input.TextArea rows={2} placeholder="请输入任务参数"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="jobStatus"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>暂停</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
      </Form>

    </Modal>
  );
};

export default CreateForm;
