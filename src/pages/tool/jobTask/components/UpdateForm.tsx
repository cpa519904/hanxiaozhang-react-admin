import React, {useEffect} from 'react';
import {Form, Input, Modal, Radio, Row, Col} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.id = updateFormValues.id;
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改任务"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='任务名称'
              name="jobName"
              rules={[{required: true, message: '请输入任务名称'}]}
            >
              <Input placeholder="请输入任务名称"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='任务分组'
              name="jobGroup"
              rules={[{required: true, message: '请输入任务分组'}]}
            >
              <Input placeholder="请输入任务分组"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='全限定名'
              name="beanClass"
              rules={[{required: true, message: '请输入全限定名'}]}
            >
              <Input placeholder="请输入全限定名"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='cron表达式'
              name="cronExpression"
              rules={[{required: true, message: '请输入cron表达式'}]}
            >
              <Input placeholder="请输入cron表达式"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='任务参数'
              name="jobParams"
            >
              <Input.TextArea rows={2} placeholder="请输入任务参数"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="jobStatus"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>暂停</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default UpdateForm;
