export interface TableListItem {

  id: number; // 定时任务主键
  jobName: string; // 任务名称
  jobGroup: string; // 任务分组
  jobStatus: number; // 任务状态，0：停止，1:运行
  cronExpression: string; // cron表达式
  beanClass: string; // 全限定名
  jobParams: string; // 任务参数
  nextExecuteDate: string; // 下一次执行时间
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
  children?: TableListItem[];
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
