import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listJobTask, {
    params,
  });
}

/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveJobTask, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateJobTask, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param id
 */
export async function remove(id: number) {
  return request(api.removeJobTask, {
    method: 'POST',
    data: {
      'id': id,
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param ids
 */
export async function batchRemove(ids: number[]) {
  return request(api.batchRemoveJobTask, {
    method: 'POST',
    data: {
      'ids': ids,
    },
    requestType: 'form',
  });
}

/**
 * 更新状态
 *
 * @param id
 * @param updateStatus
 */
export async function updateStatus(id: number, jobStatus: number) {
  return request(api.updateStatusJobTask, {
    method: 'POST',
    data: {
      'id': id,
      'jobStatus': jobStatus,
    },
    requestType: 'form',
  });
}



