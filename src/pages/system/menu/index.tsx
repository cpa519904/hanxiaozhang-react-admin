import {PlusOutlined} from '@ant-design/icons';
import {Button, Divider, message, Popconfirm} from 'antd';
import React, {useState, useRef} from 'react';
import {PageContainer, FooterToolbar} from '@ant-design/pro-layout';
import ProTable, {ProColumns, ActionType} from '@ant-design/pro-table';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import {TableListItem} from './data.d';
import {save, remove, batchRemove, list, update} from './service';
import stringUtil from '@/utils/stringUtil'
import {AuthConsumer} from '@/utils/authContent'

/**
 * 添加
 * @param fields
 */
const handleSave = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    // console.log(fields)
    const data = await save({...fields});
    hide();
    if (data.code === 1) {
      message.success('添加成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('添加失败,请重试');
    return false;
  }
  message.error('添加失败,请重试');
  return false;
};

/**
 * 更新
 * @param fields
 */
const handleUpdate = async (fields: TableListItem) => {
  const hide = message.loading('正在更新');
  try {
    const data = await update({...fields});
    hide();
    if (data.code === 1) {
      message.success('更新成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('更新失败,请重试');
    return false;
  }
  message.error('更新失败,请重试');
  return false;
};


/**
 * 删除
 * @param record
 */
const handleRemove = async (record: TableListItem) => {
  const hide = message.loading('正在删除');
  try {
    const data = await remove(record.menuId, record.parentId);
    hide();
    if (data.code === 1) {
      message.success('删除成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
  message.error('删除失败，请重试');
  return false;
};

/**
 *  批量删除
 * @param selectedRows
 */
const handleBatchRemove = async (selectedRows: TableListItem[]) => {
  const hide = message.loading('正在批量删除');
  if (!selectedRows) return true;
  try {
    const menuIds = new Array();
    const parentIds = new Array();
    selectedRows.forEach((row: TableListItem) => {
      menuIds.push(row.menuId);
      parentIds.push(row.parentId);
    })
    const data = await batchRemove(menuIds, parentIds);
    hide();
    if (data.code === 1) {
      message.success('批量删除成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('批量删除失败，请重试');
    return false;
  }
  message.error('批量删除失败，请重试');
  return false;
};


const MenuTableList: React.FC<{}> = () => {
  // 创建Model可见性
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  // 一行数据
  const [parentValues, setParentValues] = useState({});
  // 更新Model可见性
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  // 设置更新表单值
  const [updateFormValues, setUpdateFormValues] = useState({});
  // 通过actionRef进行表单刷新
  const actionRef = useRef<ActionType>();
  //  选择行数据的状态
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);

  // 删除确认按钮
  const onRemoveConfirm = async (record: TableListItem) => {
    await handleRemove(record);
    actionRef.current?.reloadAndRest?.();
  }

  // 表头
  const columns: ProColumns<TableListItem>[] = [
    {
      title: '编号',
      dataIndex: 'menuId',
      valueType: 'textarea',
      hideInForm: true,
      align: "center",
    },
    {
      title: '菜单名称',
      dataIndex: 'menuName',
      valueType: 'text',
      copyable: true,
      align: "center",
    },
    {
      title: '菜单英文名称',
      dataIndex: 'menuEnglishName',
      valueType: 'text',
      align: "center",
    },
    {
      title: '类型',
      dataIndex: 'type',
      valueType: 'select',
      align: "center",
      valueEnum: {
        0: {text: '目录',},
        1: {text: '菜单',},
        2: {text: '按钮',},
      },
    },
    {
      title: '排序',
      dataIndex: 'menuOrder',
      valueType: 'digit',
      hideInSearch: true,
      align: "center",
    },
    {
      title: '子菜单数目',
      dataIndex: 'subCount',
      valueType: 'digit',
      hideInSearch: true,
      align: "center",
    },
    {
      title: '菜单URL',
      dataIndex: 'url',
      valueType: 'textarea',
      hideInSearch: true,
      ellipsis: true,
      copyable: true,
      align: "center",
    },
    {
      title: '组件名称',
      dataIndex: 'component',
      valueType: 'textarea',
      hideInSearch: true,
      hideInTable: true,
      copyable: true,
      align: "center",
    },
    {
      title: '权限标识',
      dataIndex: 'permission',
      valueType: 'text',
      hideInSearch: true,
      hideInTable: true,
      align: "center",
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: "center",
      valueEnum: {
        0: {text: '禁用', status: 'Default'},
        1: {text: '启用', status: 'Success'},
      },
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      align: "center",
      fixed: 'right',
      render: (_, record) => (
        <>
          <AuthConsumer name={'system:menu:update'}>
            <a onClick={() => {
              handleUpdateModalVisible(true);
              setUpdateFormValues(record);
            }}
            >修改</a>
          </AuthConsumer>
          <AuthConsumer name={'system:menu:save'}>
            <Divider type="vertical"/>
            <a onClick={() => {
              handleModalVisible(true);
              setParentValues(record);
            }}
            >添加</a>
          </AuthConsumer>
          <AuthConsumer name={'system:menu:remove'}>
            <Divider type="vertical"/>
            <Popconfirm
              title="确认需要删除"
              okText="确定"
              cancelText="取消"
              onConfirm={() => onRemoveConfirm(record)}
            >
              <a href="#">删除</a>
            </Popconfirm>
          </AuthConsumer>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      {/* 表格 */}
      <ProTable<TableListItem>
        headerTitle=""
        actionRef={actionRef}
        rowKey="menuId"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <AuthConsumer name={'system:menu:save'}>
            <Button type="primary" onClick={() => handleModalVisible(true)}>
              <PlusOutlined/> 添加
            </Button>
          </AuthConsumer>,
        ]}
        // request = {async (params, sorter, filter) => {
        request={async (params) => {
          // console.log(params.deptName)
          // console.log(JSON.stringify(params.deptName))
          // console.log(stringUtil.isEmpty(params.deptName))
          if (JSON.stringify(params) === "{}"
            || (stringUtil.isEmpty(params.deptId)
              && stringUtil.isEmpty(params.deptName))
          ) {
            params.parentId = 0
          }
          const data = await list({...params});
          if (data.code === 1) {
            return {
              data: data.data,
              // success请返回true，不然table会停止解析数据，即使有数据
              success: true,
              // 不传会使用 data 的长度，如果是分页一定要传
              // total: data.data.length,
            };
          }
          return {
            data: [],
            success: true,
          };
        }
        }
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => setSelectedRows(selectedRows),
        }}
        pagination={false} // 树形数据关闭分页
      />

      {/* 选择数据的状态 */}
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择 <a style={{fontWeight: 600}}>{selectedRowsState.length}</a> 项&nbsp;&nbsp;
            </div>
          }
        >
          <AuthConsumer name={'system:menu·:batchRemove'}>
            <Button danger type={"primary"}
                    onClick={async () => {
                      await handleBatchRemove(selectedRowsState);
                      setSelectedRows([]);
                      actionRef.current?.reloadAndRest?.();
                    }}
            >
              批量删除
            </Button>
          </AuthConsumer>
        </FooterToolbar>
      )}

      {/* 创建表单 */}
      <CreateForm
        handleModalVisible={handleModalVisible}
        modalVisible={createModalVisible}
        parentValues={parentValues}
        handleSave={handleSave}
        actionRef={actionRef}
      ></CreateForm>

      {/* 更新表单 */}
      <UpdateForm
        updateModalVisible={updateModalVisible}
        handleUpdateModalVisible={handleUpdateModalVisible}
        updateFormValues={updateFormValues}
        handleUpdate={handleUpdate}
        actionRef={actionRef}
      ></UpdateForm>


    </PageContainer>
  );
};

export default MenuTableList;
