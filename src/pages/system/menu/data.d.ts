export interface TableListItem {
  menuId: number; // 菜单主键
  parentId: number; // 父菜单ID，一级菜单为0
  parentName: number; // 上级菜单名称
  subCount: number; // 子菜单数目
  menuName: string; // 菜单名称
  menuEnglishName: string; // 菜单英文名称
  menuOrder: number; // 排序
  url: string; // 菜单URL
  component: string; // 组件
  permission: string; // 授权
  type: number; // 类型   0：目录   1：菜单   2：按钮
  icon: string; // 菜单图标
  status: number; // 状态 0:禁用，1:正常
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
  children?: TableListItem[];
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
