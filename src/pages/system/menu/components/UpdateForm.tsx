import React, {useEffect} from 'react';
import {Form, Input, Modal, Radio, Row, Col, InputNumber} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.menuId = updateFormValues.menuId;
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改菜单"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='上级菜单'
              name="parentName"
            >
              <Input disabled={true}/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单名称'
              name="menuName"
              rules={[{required: true, message: '请输入菜单名称'}]}
            >
              <Input placeholder="请输入菜单名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单英文名称'
              name="menuEnglishName"
            >
              <Input placeholder="请输入菜单英文名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='类型'
              name="type"
              rules={[{required: true, message: '请选择类型'}]}
            >
              <Radio.Group>
                <Radio value={0}>目录</Radio>
                <Radio value={1}>菜单</Radio>
                <Radio value={2}>按钮</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单URL'
              name="url"
            >
              <Input placeholder="请输入菜单URL"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='组件名称'
              name="component"
            >
              <Input placeholder="请输入组件名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='权限标识'
              name="permission"
            >
              <Input placeholder="请输入权限标识"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='菜单图标'
              name="icon"
            >
              <Input placeholder="请输入菜单图标" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='菜单排序'
              name="menuOrder"
            >
              <InputNumber placeholder="菜单排序"/>
            </Form.Item>
          </Col>

        </Row>
      </Form>
    </Modal>
  );
};

export default UpdateForm;
