import React, {useEffect} from 'react';
import {Modal, Form, Row, Col, Input, InputNumber, Radio} from 'antd';
import {TableListItem} from "../data.d";

interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  parentValues: any;
  handleSave: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible, parentValues, handleSave, actionRef} = props;

  const [form] = Form.useForm();

  useEffect(() => {
    console.log(parentValues)
    if (JSON.stringify(parentValues) !== "{}") {
      form.setFieldsValue({parentName: parentValues.menuName});
      form.setFieldsValue({type: parentValues.type === 0 ? 1 : 2});
    }
  }, [parentValues]);

  const onCancel = () => {
    form.resetFields();
    handleModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.parentId = JSON.stringify(parentValues) === "{}" ? 0 : parentValues.menuId;
      // console.log(values)
      const success = await handleSave(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      destroyOnClose
      title="新建菜单"
      visible={modalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >
      <Form className="addForm"
            initialValues={{parentName: "无", status: 1, type: 0}}
            form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='上级菜单'
              name="parentName"
            >
              <Input disabled={true}/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单名称'
              name="menuName"
              rules={[{required: true, message: '请输入菜单名称'}]}
            >
              <Input placeholder="请输入菜单名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单英文名称'
              name="menuEnglishName"
            >
              <Input placeholder="请输入菜单英文名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='类型'
              name="type"
              rules={[{required: true, message: '请选择类型'}]}
            >
              <Radio.Group>
                <Radio value={0}>目录</Radio>
                <Radio value={1}>菜单</Radio>
                <Radio value={2}>按钮</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='菜单URL'
              name="url"
            >
              <Input placeholder="请输入菜单URL"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='组件名称'
              name="component"
            >
              <Input placeholder="请输入组件名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='权限标识'
              name="permission"
            >
              <Input placeholder="请输入权限标识"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='菜单图标'
              name="icon"
            >
              <Input placeholder="请输入菜单图标"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='菜单排序'
              name="menuOrder"
            >
              <InputNumber placeholder="菜单排序"/>
            </Form.Item>
          </Col>

        </Row>

      </Form>

    </Modal>
  );
};

export default CreateForm;
