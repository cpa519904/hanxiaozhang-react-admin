import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listMenu, {
    params,
  });
}

/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveMenu, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateMenu, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param menuId
 * @param parentId
 */
export async function remove(menuId: number, parentId: number) {
  return request(api.removeMenu, {
    method: 'POST',
    data: {
      'menuId': menuId,
      'parentId': parentId
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param menuIds
 * @param parentIds
 */
export async function batchRemove(menuIds: number[],parentIds: number[]) {
  return request(api.batchRemoveMenu, {
    method: 'POST',
    data: {
      'menuIds': menuIds,
      'parentIds': parentIds
    },
    requestType: 'form',
  });
}

