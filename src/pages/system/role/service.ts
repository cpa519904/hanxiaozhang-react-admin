import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 菜单树
 *
 */
export async function menuTree(): Promise<any> {
  return request(api.menuTree);
}

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listRole, {
    params,
  });
}

/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateRole, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveRole, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param roleId
 */
export async function remove(roleId: number) {
  return request(api.removeRole, {
    method: 'POST',
    data: {
      'roleId': roleId,
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param roleIds
 */
export async function batchRemove(roleIds: number[]) {
  return request(api.batchRemoveRole, {
    method: 'POST',
    data: {
      'roleIds': roleIds,
    },
    requestType: 'form',
  });
}

