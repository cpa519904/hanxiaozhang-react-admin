export interface TableListItem {
  roleId: number; // 角色主键
  roleName: string; // 角色名称
  roleSign: string; // 角色标识
  status: number; // 状态 0:禁用，1:正常
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
  menuIds: number[] // 菜单id
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}


interface MenuTree {
  key: number;
  title: string;
  disabled: boolean;
  children: MenuTree[];
}
