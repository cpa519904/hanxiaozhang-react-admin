import React, {useEffect, useState} from 'react';
import {Form, Input, Modal, Radio, Row, Col, Card, Tree, message} from 'antd';
import {MenuTree, TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  menuTreeValues: MenuTree[];
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, menuTreeValues, updateFormValues, handleUpdate, actionRef} = props;

  const [treeCheckedKeys, setTreeCheckedKeys] = useState<React.Key[]>([]);
  const [treeVisible, setTreeVisible] = useState<boolean>(false);

  const [form] = Form.useForm();

  useEffect(() => {
    if (menuTreeValues.length > 0) {
      // console.log(menuTreeValues);
      setTreeVisible(true)
    }
  }, [menuTreeValues]);

  useEffect(() => {
    if (updateModalVisible === true) {
      form.setFieldsValue({...updateFormValues});
      // console.log(updateFormValues.menuIds)
      setTreeCheckedKeys(updateFormValues.menuIds);
    }

  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      if (treeCheckedKeys.length === 0) {
        message.error('请勾选菜单，菜单不能为空!');
        return;
      }
      console.log(treeCheckedKeys)
      values.roleId = updateFormValues.roleId;
      values.menuIds = treeCheckedKeys;
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  const onCheckTree = (checkedKeys: React.Key[]) => {
    // console.log(checkedKeys['checked'])
    setTreeCheckedKeys(checkedKeys['checked'])
  };

  return (
    <Modal
      forceRender
      title="修改角色"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Card title={"角色设置"}
            size={"small"}>
        <Form className="addForm"
              initialValues={{status: 1}}
              form={form}
              layout="vertical">

          <Row gutter={24}>
            <Col span={24}>
              <Form.Item
                label='角色名称'
                name="roleName"
                rules={[{required: true, message: '请输入角色名称'}]}
              >
                <Input placeholder="请输入角色名称"/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col span={12}>
              <Form.Item
                label='状态'
                name="status"
                rules={[{required: true, message: '请选择状态'}]}
              >
                <Radio.Group>
                  <Radio value={1}>启用</Radio>
                  <Radio value={0}>禁用</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>

      <Card title={"选择菜单"}
        // headStyle={{backgroundColor: '#69c0ff', color: 'white'}}
            style={{marginTop: 16}}
            size={"small"}>
        {treeVisible &&
        <Tree
          checkable
          defaultExpandAll={true}  // 默认展开所有树节点
          checkStrictly={true} // checkable 状态下节点选择完全受控（父子节点选中状态不再关联）
          checkedKeys={treeCheckedKeys}  // 选中复选框的树节点
          height={260}
          onCheck={onCheckTree}
          treeData={menuTreeValues}
        >
        </Tree>}

      </Card>
    </Modal>
  );
};

export default UpdateForm;
