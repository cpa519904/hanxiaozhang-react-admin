import React, {useState, useEffect} from 'react';
import {Modal, Form, Row, Col, Input, Card, Radio, Tree, message} from 'antd';
import {MenuTree, TableListItem} from "../data.d";


interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  menuTreeValues: MenuTree[];
  handleSave: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible, menuTreeValues, handleSave, actionRef} = props;

  const [form] = Form.useForm();
  const [treeCheckedKeys, setTreeCheckedKeys] = useState<React.Key[]>([]);
  const [treeVisible, setTreeVisible] = useState<boolean>(false);

  useEffect(() => {
    if (menuTreeValues.length > 0) {
      setTreeVisible(true)
      setTreeCheckedKeys([]);
    }
  }, [menuTreeValues]);

  const onCancel = () => {
    form.resetFields();
    handleModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      // console.log(values)
      if (treeCheckedKeys.length === 0) {
        message.error('请勾选菜单，菜单不能为空!');
        return;
      }
      // console.log(treeCheckedKeys)
      values.menuIds = treeCheckedKeys;
      const success = await handleSave(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  const onCheckTree = (checkedKeys: React.Key[]) => {
    // console.log(info)
    setTreeCheckedKeys(checkedKeys)
  };

  return (
    <Modal
      forceRender
      destroyOnClose
      title="新建角色"
      visible={modalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >
      <Card title={"角色设置"}
            size={"small"}>
        <Form className="addForm"
              initialValues={{status: 1}}
              form={form}
              layout="vertical">

          <Row gutter={24}>
            <Col span={24}>
              <Form.Item
                label='角色名称'
                name="roleName"
                rules={[{required: true, message: '请输入角色名称'}]}
              >
                <Input placeholder="请输入角色名称"/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={24}>
            <Col span={12}>
              <Form.Item
                label='状态'
                name="status"
                rules={[{required: true, message: '请选择状态'}]}
              >
                <Radio.Group>
                  <Radio value={1}>启用</Radio>
                  <Radio value={0}>禁用</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>

      <Card title={"选择菜单"}
        // headStyle={{backgroundColor: '#69c0ff', color: 'white'}}
            style={{marginTop: 16}}
            size={"small"}>
        {treeVisible &&
        <Tree
          checkable
          defaultExpandAll={true}
          checkedKeys={treeCheckedKeys}  // 选中复选框的树节点
          height={260}
          onCheck={onCheckTree}
          treeData={menuTreeValues}
        >
        </Tree>}

      </Card>

    </Modal>
  );
};


export default CreateForm;
