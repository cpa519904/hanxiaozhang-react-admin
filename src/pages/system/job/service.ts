import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listJob, {
    params,
  });
}

/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateJob, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param jobId
 */
export async function remove(jobId: number) {
  return request(api.removeJob, {
    method: 'POST',
    data: {
      'jobId': jobId,
    },
    requestType: 'form',
  });
}


/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveJob, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param jobIds
 */
export async function batchRemove(jobIds: number[]) {
  return request(api.batchRemoveJob, {
    method: 'POST',
    data: {
      'jobIds': jobIds,
    },
    requestType: 'form',
  });
}

