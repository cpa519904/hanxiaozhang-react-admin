export interface TableListItem {
  deptId: number; // 部门主键
  deptName: string;  // 部门名称
  parentId: number; // 上级部门ID，一级部门为0
  parentName: number; // 上级部门名称
  deptOrder: number; // 排序
  subCount: number; // 子部门数目
  status: number; // 状态 0:禁用，1:正常
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
  children?: TableListItem[];
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
