import React, {useEffect} from 'react';
import {Modal, Form, Row, Col, Input, InputNumber, Radio} from 'antd';
import {TableListItem} from "../data.d";

interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  parentValues: any;
  handleSave: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible, parentValues, handleSave, actionRef} = props;

  const [form] = Form.useForm();

  useEffect(() => {
    if (JSON.stringify(parentValues) !== "{}") {
      form.setFieldsValue({parentName: parentValues.deptName});
    }
  }, [parentValues]);

  const onCancel = () => {
    form.resetFields();
    handleModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.parentId = JSON.stringify(parentValues) === "{}" ? 0 : parentValues.deptId;
      // console.log(values)
      const success = await handleSave(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      destroyOnClose
      title="新建部门"
      visible={modalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >
      <Form className="addForm"
            initialValues={{parentName: "无", status: 1}}
            form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='上级部门'
              name="parentName"
            >
              <Input disabled={true}/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='部门名称'
              name="deptName"
              rules={[{required: true, message: '请输入部门名称'}]}
            >
              <Input placeholder="请输入部门名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='部门排序'
              name="deptOrder"
              rules={[{required: true, message: '请输入部门排序'}]}
            >
              <InputNumber placeholder="部门排序"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

      </Form>

    </Modal>
  );
};

export default CreateForm;
