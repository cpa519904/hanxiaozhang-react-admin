import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listDept, {
    params,
  });
}

/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveDept, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateDept, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param deptId
 * @param parentId
 */
export async function remove(deptId: number, parentId: number) {
  return request(api.removeDept, {
    method: 'POST',
    data: {
      'deptId': deptId,
      'parentId': parentId
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param deptIds
 * @param parentIds
 */
export async function batchRemove(deptIds: number[],parentIds: number[]) {
  return request(api.batchRemoveDept, {
    method: 'POST',
    data: {
      'deptIds': deptIds,
      'parentIds': parentIds
    },
    requestType: 'form',
  });
}

