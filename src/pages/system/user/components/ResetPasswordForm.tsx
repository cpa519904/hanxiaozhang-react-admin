import React from 'react';
import {Form, Input, Modal, Row, Col} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  resetPasswordModalVisible: boolean;
  handleResetPasswordModalVisible: any;
  updateFormValues: TableListItem | undefined;
  handleResetPassword: (userId: number, passwordOne: string, passwordTwo: string) => Promise<boolean>;
  actionRef: any;
}


const ResetPasswordForm: React.FC<UpdateFormProps> = (props) => {

  const {resetPasswordModalVisible, handleResetPasswordModalVisible, updateFormValues, handleResetPassword, actionRef} = props;


  const [form] = Form.useForm();


  const onCancel = () => {
    form.resetFields();
    handleResetPasswordModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: any) => {
      const success = await handleResetPassword((updateFormValues as TableListItem).userId, values.passwordOne, values.passwordOne);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="重置密码"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={resetPasswordModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="resetPasswordForm" form={form}
            layout="vertical"
      >

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='新密码'
              name="passwordOne"
              rules={[{required: true, message: '请输入新密码'}]}
            >
              <Input type={"password"} placeholder="请输入新密码"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='确认新密码'
              name="passwordTwo"
              rules={[{required: true, message: '请输入新密码'}]}
            >
              <Input type={"password"} placeholder="请输入新密码"/>
            </Form.Item>
          </Col>
        </Row>

      </Form>

    </Modal>
  );
};

export default ResetPasswordForm;
