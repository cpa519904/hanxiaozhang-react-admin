import React, {useEffect} from 'react';
import {Form, Input, Modal, Radio, Row, Col, TreeSelect, Select} from 'antd';
import {TableListItem} from "../data.d";
import {TableListItem as RoleData} from "@/pages/system/role/data.d";
import {TableListItem as JobData} from "@/pages/system/job/data.d";

const {Option} = Select;


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: TableListItem | undefined;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  deptTreeValues: [];
  roleValues: RoleData[];
  jobValues: JobData[];
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, deptTreeValues, roleValues, jobValues, actionRef} = props;

  const [form] = Form.useForm();

  const getRoleOption = (list: RoleData[]) => {
    if (!list || list.length < 1) {
      return (
        <></>
      );
    }
    return list.map((item) => (
      <Option key={item.roleId} value={item.roleId}>
        {item.roleName}
      </Option>
    ));
  };

  const getJobOption = (list: JobData[]) => {
    if (!list || list.length < 1) {
      return (
        <></>
      );
    }
    return list.map((item) => (
      <Option key={item.jobId} value={item.jobId}>
        {item.jobName}
      </Option>
    ));
  };

  useEffect(() => {
    // console.log(updateFormValues)
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.userId = updateFormValues.userId;
      // console.log(values)
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改用户"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >


        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='用户名'
              name="username"
              rules={[{required: true, message: '请输入用户名'}]}
            >
              <Input placeholder="请输入用户名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='姓名'
              name="name"
              rules={[{required: true, message: '请输入姓名'}]}
            >
              <Input placeholder="请输入姓名"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='部门'
              name="deptIds"
              rules={[{required: true, message: '请选择部门'}]}
            >
              <TreeSelect
                multiple
                treeData={deptTreeValues}
                placeholder="请选择部门"
                treeDefaultExpandAll
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='角色'
              name="roleIds"
              rules={[{required: true, message: '请选择角色'}]}
            >
              <Select
                mode="multiple"
                placeholder="请选择角色">
                {getRoleOption(roleValues)}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='岗位'
              name="jobIds"
              rules={[{required: true, message: '请选择岗位'}]}
            >
              <Select
                mode="multiple"
                placeholder="请选择岗位">
                {getJobOption(jobValues)}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='性别'
              name="sex"
            >
              <Radio.Group>
                <Radio value={0}>男</Radio>
                <Radio value={1}>女</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='手机号码'
              name="phone"
            >
              <Input placeholder="手机号码"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='邮箱'
              name="email"
            >
              <Input placeholder="请输入邮箱"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='备注'
              name="remarks"
            >
              <Input.TextArea rows={2} placeholder="请输入备注"/>
            </Form.Item>
          </Col>
        </Row>

      </Form>

    </Modal>
  );
};

export default UpdateForm;
