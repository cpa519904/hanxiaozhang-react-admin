import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 部门树
 *
 */
export async function deptTree(): Promise<any> {
  return request(api.deptTree);
}

/**
 * 角色
 *
 */
export async function listRole(): Promise<any> {
  return request(api.listRole, {
    method: 'GET',
    params: {current: 1, pageSize: 1000}
  });
}

/**
 * 岗位
 *
 */
export async function listJob(): Promise<any> {
  return request(api.listJob, {
    method: 'GET',
    params: {current: 1, pageSize: 1000}
  });
}


/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listUser, {
    params,
  });
}


/**
 * 重置密码
 *
 * @param userId
 * @param passwordOne
 * @param passwordTwo
 */
export async function resetPassword(userId: number, passwordOne: string, passwordTwo: string) {
  return request(api.resetPassword, {
    method: 'POST',
    data: {
      'userId': userId,
      'passwordOne': passwordOne,
      'passwordTwo': passwordTwo
    },
  });
}


/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateUser, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param userId
 */
export async function remove(userId: number) {
  return request(api.removeUser, {
    method: 'POST',
    data: {
      'userId': userId,
    },
    requestType: 'form',
  });
}


/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveUser, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param userIds
 */
export async function batchRemove(userIds: number[]) {
  return request(api.batchRemoveUser, {
    method: 'POST',
    data: {
      'userIds': userIds,
    },
    requestType: 'form',
  });
}

