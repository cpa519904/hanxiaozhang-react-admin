export interface TableListItem {
  userId: number; // 用户主键
  username: string; // 用户名
  name: string; // 名称
  nickName: string; // 昵称
  sex: number; // 性别
  birth: string; // 出身日期
  phone: string; // 手机号码
  email: string; // 邮箱
  status: number; // 状态 0:禁用，1:正常
  country: number; // 国家
  province: number; // 省
  city: number; // 市
  county: number; // 县
  address: string; // 地址
  headHeadPath: string; // 头像地址
  signature: string; // 个性签名
  introduction: string; // 个人简介
  type: number; // 类型
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
  deptIds: number[]; // 部门
  jobIds: number[];  // 岗位
  roleIds: number[]; // 角色
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}

interface TreeItem {
  key: number | null;
  value: number | null;
  title: string;
  disabled: boolean;
  children: TreeItem[];
}
