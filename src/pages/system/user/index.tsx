import {PlusOutlined} from '@ant-design/icons';
import {Button, Divider, message, Popconfirm, Card, Tree} from 'antd';
import React, {useState, useRef, useEffect} from 'react';
import {PageContainer, FooterToolbar} from '@ant-design/pro-layout';
import ProTable, {ProColumns, ActionType} from '@ant-design/pro-table';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import ResetPasswordForm from './components/ResetPasswordForm';
import {TableListItem} from './data.d';
import {save, remove, batchRemove, list, update, deptTree, listRole, listJob, resetPassword} from './service';
import {AuthConsumer} from '@/utils/authContent'
import {Key} from "rc-tree/lib/interface";
import {TableListItem as RoleData} from "@/pages/system/role/data.d";
import {TableListItem as JobData} from "@/pages/system/job/data.d";


/**
 * 获得部门树
 */
const getDeptTree = async () => {
  try {
    const data = await deptTree();
    if (data.code === 1) {
      return data.data;
    }
  } catch (error) {
    message.error('获取部门树失败,请重试');
    return [];
  }
  message.error('获取部门树失败,请重试');
  return [];
}
/**
 * 获得角色
 */
const getRole = async () => {
  try {
    const data = await listRole();
    if (data.code === 1) {
      return data.data.rows;
    }
  } catch (error) {
    message.error('获取角色失败,请重试');
    return [];
  }
  message.error('获取角色树失败,请重试');
  return [];
}

/**
 * 岗位
 */
const getJob = async () => {
  try {
    const data = await listJob();
    if (data.code === 1) {
      return data.data.rows;
    }
  } catch (error) {
    message.error('获取岗位树失败,请重试');
    return [];
  }
  message.error('获取岗位树失败,请重试');
  return [];
}


/**
 * 添加
 * @param fields
 */
const handleSave = async (fields: TableListItem) => {
  const hide = message.loading('正在添加');
  try {
    // console.log(fields)
    const data = await save({...fields});
    hide();
    if (data.code === 1) {
      message.success('添加成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('添加失败,请重试');
    return false;
  }
  message.error('添加失败,请重试');
  return false;
};

/**
 * 更新
 * @param fields
 */
const handleUpdate = async (fields: TableListItem) => {
  const hide = message.loading('正在更新');
  try {
    const data = await update({...fields});
    hide();
    if (data.code === 1) {
      message.success('更新成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('更新失败,请重试');
    return false;
  }
  message.error('更新失败,请重试');
  return false;
};


/**
 * 重置密码
 *
 * @param userId
 * @param passwordOne
 * @param passwordTwo
 */
const handleResetPassword = async (userId: number, passwordOne: string, passwordTwo: string) => {
  const hide = message.loading('正在重置密码');
  try {
    const data = await resetPassword(userId, passwordOne, passwordTwo);
    hide();
    if (data.code === 1) {
      message.success('重置密码成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('重置密码失败,请重试');
    return false;
  }
  message.error('重置密码失败,请重试');
  return false;
};


/**
 * 删除
 * @param record
 */
const handleRemove = async (record: TableListItem) => {
  const hide = message.loading('正在删除');
  try {
    const data = await remove(record.userId);
    hide();
    if (data.code === 1) {
      message.success('删除成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
  message.error('删除失败，请重试');
  return false;
};

/**
 *  批量删除
 * @param selectedRows
 */
const handleBatchRemove = async (selectedRows: TableListItem[]) => {
  const hide = message.loading('正在批量删除');
  if (!selectedRows) return true;
  try {
    const userIds = new Array();
    selectedRows.forEach((row: TableListItem) => {
      userIds.push(row.userId);
    })
    const data = await batchRemove(userIds);
    hide();
    if (data.code === 1) {
      message.success('批量删除成功，即将刷新');
      return true;
    }
  } catch (error) {
    hide();
    message.error('批量删除失败，请重试');
    return false;
  }
  message.error('批量删除失败，请重试');
  return false;
};


const UserTableList: React.FC<{}> = () => {
  // 创建Model可见性
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  // 更新Model可见性
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  // 重置密码Model可见性
  const [resetPasswordModalVisible, handleResetPasswordModalVisible] = useState<boolean>(false);
  // 设置更新表单值
  const [updateFormValues, setUpdateFormValues] = useState<TableListItem>();
  //  选择行数据的状态
  const [selectedRowsState, setSelectedRows] = useState<TableListItem[]>([]);
  // 部门树的数据
  const [deptTreeValues, setDeptTreeValues] = useState<[]>([]);
  // 部门树是否显示
  const [deptTreeVisible, setDeptTreeVisible] = useState<boolean>(false);
  // 部门id
  const [deptId, setDeptId] = useState<number>();
  // 通过actionRef进行表单刷新
  const actionRef = useRef<ActionType>();
  // 角色的数据
  const [roleValues, setRoleValues] = useState<RoleData[]>([]);
  // 岗位的数据
  const [jobValues, setJobValues] = useState<JobData[]>([]);

  useEffect(() => {
    const deptTreeData = async () => {
      const result = await getDeptTree();
      setDeptTreeValues(result);
    }
    deptTreeData();
  }, []);

  useEffect(() => {
    if (deptTreeValues.length > 0) {
      setDeptTreeVisible(true)
    }
  }, [deptTreeValues]);

  // 点击按钮状态
  const onClickModalVisible = (saveFlag: boolean) => {
    if (saveFlag) {
      handleModalVisible(true);
    } else {
      handleUpdateModalVisible(true);
    }
    getRole().then(data => {
      setRoleValues(data);
    })
    getJob().then(data => {
      setJobValues(data);
    })
  }

  // 删除确认按钮
  const onRemoveConfirm = async (record: TableListItem) => {
    await handleRemove(record);
    actionRef.current?.reloadAndRest?.();
  }

  // 选择树的节点信息
  const onSelectTree = (selectedKeys: Key[]) => {
    setDeptId(selectedKeys[0] as number)
  }

  // 表头
  const columns: ProColumns<TableListItem>[] = [
    {
      title: '编号',
      dataIndex: 'userId',
      valueType: 'textarea',
      hideInForm: true,
      align: "center",
    },
    {
      title: '姓名',
      dataIndex: 'name',
      valueType: 'text',
      copyable: true,
      align: "center",
    },
    {
      title: '用户名',
      dataIndex: 'username',
      valueType: 'text',
      copyable: true,
      align: "center",
    },
    {
      title: '性别',
      dataIndex: 'sex',
      align: "center",
      valueEnum: {
        0: {text: '男',},
        1: {text: '女',},
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      align: "center",
      valueEnum: {
        0: {text: '禁用', status: 'Default'},
        1: {text: '启用', status: 'Success'},
      },
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      align: "center",
      render: (_, record) => (
        <>
          <AuthConsumer name={'system:user:update'}>
            <a onClick={() => {
              onClickModalVisible(false);
              setUpdateFormValues(record);
            }}
            >修改</a>
          </AuthConsumer>
          <AuthConsumer name={'system:user:resetPassword'}>
            <Divider type="vertical"/>
            <a onClick={() => {
              handleResetPasswordModalVisible(true);
              setUpdateFormValues(record);
            }}
            >重置密码</a>
          </AuthConsumer>
          <AuthConsumer name={'system:user:remove'}>
            <Divider type="vertical"/>
            <Popconfirm
              title="确认需要删除"
              okText="确定"
              cancelText="取消"
              onConfirm={() => onRemoveConfirm(record)}
            >
              <a href="#">删除</a>
            </Popconfirm>
          </AuthConsumer>
        </>
      ),
    },
  ];

  return (
    <PageContainer>
      {/* 表格 */}
      <ProTable<TableListItem>
        headerTitle=""
        actionRef={actionRef}
        rowKey="userId"
        search={{
          labelWidth: 120,
        }}
        // 工具栏
        toolBarRender={() => [
          <AuthConsumer name={'system:user:save'}>
            <Button type="primary" onClick={() => onClickModalVisible(true)}>
              <PlusOutlined/> 添加
            </Button>
          </AuthConsumer>,
        ]}
        // 表格主题
        tableRender={(_, dom) => (
          <div style={{display: 'flex', width: '100%',}}>
            <Card title={'选择部门'}
                  headStyle={{fontSize: 14}}
            >
              {deptTreeVisible &&
              <Tree
                defaultExpandAll={true}
                height={600}
                onSelect={onSelectTree}
                treeData={deptTreeValues}
              ></Tree>}
            </Card>
            {/* 表格主体 */}
            <div style={{flex: 1,}}>
              {dom}
            </div>
          </div>
        )}
        params={{
          deptId
        }}
        // request = {async (params, sorter, filter) => {
        request={async (params) => {
          // console.log(params)
          const data = await list({...params});
          if (data.code === 1) {
            return {
              data: data.data.rows,
              // success请返回true，不然table会停止解析数据，即使有数据
              success: true,
              // 不传会使用 data 的长度，如果是分页一定要传
              total: data.data.total,
            };
          }
          return {
            data: [],
            success: true,
            total: 0,
          }
        }
        }
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => setSelectedRows(selectedRows),
        }}
      />

      {/* 选择数据的状态 */}
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择 <a style={{fontWeight: 600}}>{selectedRowsState.length}</a> 项&nbsp;&nbsp;
            </div>
          }
        >
          <AuthConsumer name={'system:user:batchRemove'}>
            <Button danger type={"primary"}
                    onClick={async () => {
                      await handleBatchRemove(selectedRowsState);
                      setSelectedRows([]);
                      actionRef.current?.reloadAndRest?.();
                    }}
            >
              批量删除
            </Button>
          </AuthConsumer>
        </FooterToolbar>
      )}

      {/* 创建表单 */}
      <CreateForm
        handleModalVisible={handleModalVisible}
        modalVisible={createModalVisible}
        handleSave={handleSave}
        deptTreeValues={deptTreeValues}
        roleValues={roleValues}
        jobValues={jobValues}
        actionRef={actionRef}
      ></CreateForm>

      {/* 更新表单 */}
      <UpdateForm
        updateModalVisible={updateModalVisible}
        handleUpdateModalVisible={handleUpdateModalVisible}
        updateFormValues={updateFormValues}
        handleUpdate={handleUpdate}
        deptTreeValues={deptTreeValues}
        roleValues={roleValues}
        jobValues={jobValues}
        actionRef={actionRef}
      ></UpdateForm>

      {/* 重置密码表单 */}
      <ResetPasswordForm
        resetPasswordModalVisible={resetPasswordModalVisible}
        handleResetPasswordModalVisible={handleResetPasswordModalVisible}
        updateFormValues={updateFormValues}
        handleResetPassword={handleResetPassword}
        actionRef={actionRef}
      ></ResetPasswordForm>

    </PageContainer>
  );
};

export default UserTableList;
