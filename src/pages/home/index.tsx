import {Card, Col, List, Row, Tag} from "antd";
import React from "react";
import {ListItemDataType, TagDataType} from "./data.d";
import styles from "./index.less";


/**
 * 介绍信息
 */
const introduceList: ListItemDataType[] = [
  {
    id: '1',
    title: '系统简介：',
    tags: [
      {
        colour: 'magenta',
        label: '开源'
      },
      {
        label: '学习'
      },
      {
        colour: 'purple',
        label: '便捷'
      },
    ],
    contents:
      <div>
        <div>
          <div className={styles['status-point']}></div>
          hanxiaozhang管理系统是前后端分离的开源项目。前端使用React+Ant Design Pro进行开发，后端使用Springcloud微服务框架进行开发。
        </div>
        <div>
          <div className={styles['status-point']}></div>
          hanxiaozhang管理系统主要包括登录管理、基础管理、系统管理、工具管理、图形编辑器、用户中心等功能，避免开发人员重复造轮子，减少开发量（部分功能为开发完成）。
        </div>
        <div>
          <div className={styles['status-point']}></div>
          hanxiaozhang管理系统相关功能由作者独自完成，一是为了提高开发技能，二是希望可以帮助其他人，并能结交志同道合的朋友，有任何相关问题，大家可以发送邮件至hanxiaozhang2018@sina.com。
        </div>
      </div>
  },
  {
    id: '2',
    title: '系统功能：',
    tags: [
      {
        colour: 'magenta',
        label: '登录管理'
      },
      {
        label: '首页'
      },
      {
        colour: 'magenta',
        label: '基础管理'
      },
      {
        colour: 'cyan',
        label: '系统管理'
      },
      {
        colour: 'geekblue',
        label: '工具管理'
      },
      {
        colour: 'volcano',
        label: '图形编辑器'
      },
      {
        colour: 'red',
        label: '用户中心'
      },],
    contents:
      <div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            首页：
          </h4>
          <div>
            介绍该系统相关功能。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            登录管理：
          </h4>
          <div>
            登录：使用账户密码+验证码的方式进行登录。安全方面使用Oauth2.0 + Spring Security保障系统的安全。
          </div>
          <div>
            登出：登出操作可以清除浏览器端Localstorage缓存和后端数据Token信息。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            基础管理：
          </h4>
          <div>
            字典管理：实现字典数据的增删查改、批量删除。
          </div>
          <div>
            日志管理：实现日志数据的增删查，支持切面注解方式为相关方法保存日志到数据库（@LogAnnotation），执行切面方式打印日志到控制台。
          </div>
          <div>
            文件管理：实现文件数据的增删查改，并且支持预览（只读文件预览有些问题，待解决），逻辑可以参考：<a href={'https://blog.csdn.net/huantai3334/article/details/105874448'} target={'_blank'}>《基于Springboot+Bootstrap的文件管理全套解决方案（文件列表、上传、预览、下载、删除、打包下载等）》</a>。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            系统管理:
          </h4>
          <div>
            部门管理：实现部门数据的增删查改、批量删除，支持树结构展示数据。
          </div>
          <div>
            岗位管理：实现岗位数据的增删查改、批量删除。
          </div>
          <div>
            菜单管理：实现菜单数据的增删查改、批量删除，支持树结构展示数据和按钮级别权限标识的管理。
          </div>
          <div>
            角色管理：实现角色数据的增删查改、批量删除，支持角色选择需要的菜单；支持角色选择需要的数据权限（待开发）。
          </div>
          <div>
            用户管理：实现用户数据的增删查改、批量删除、重置密码，支持用户选择相应的部门、岗位和角色。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            工具管理：
          </h4>
          <div>
            代码生成器：根据表结构自动生成前后端代码，减少开发量。
          </div>
          <div>
            定时任务：使用quartz框架管理系统需要维护的定时任务，实现定时任务的增删查改、批量删除、暂定与开启，该部分参考了<a href={'https://blog.csdn.net/qq_48008521/article/details/108800849'} target={'_blank'}>《SpringBoot2.x集成Quartz实现定时任务管理（持久化到数据库）》</a>。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            图形编辑器：
          </h4>
          <div>
            流程编辑器：支持在线编辑流程图。
          </div>
          <div>
            脑图编辑器：支持在线脑图流程图。
          </div>
          <div>
            拓扑编辑器：支持在线拓扑流程图。
          </div>
        </div>

        <div>
          <h4>
            <div className={styles['status-point']}></div>
            用户中心：
          </h4>
          <div>
            用户设置：支持用户修改自己个人信息。
          </div>
          <div>
            用户重置密码：支持用户重置密码。
          </div>
        </div>
      </div>
  },
  {
    id: '3',
    title: '技术栈：',
    tags: [
      {
        colour: 'cyan',
        label: 'SpringCloud'
      },
      {
        colour: 'orange',
        label: 'MyBatis'
      },
      {
        colour: 'magenta',
        label: 'Docker'
      },
      {
        colour: 'lime',
        label: 'React'
      },
      {
        colour: 'geekblue',
        label: 'TypeScript'
      },
      {
        label: 'Ant Design Pro'
      },],
    contents:
      <div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            后端（Java）：
          </h4>
          <div>
            微服务框架：Spring
            Cloud，主要组件包括：服务中心：Eureka、配置中心：Config、网关路由：Zuul、负载均衡器：Ribbon、断路器：Hystrix、RestFul远程调用：Fegin等，
            相关知识可以参考：<a href={'https://blog.csdn.net/huantai3334/article/details/110159646'} target={'_blank'}>《hanxiaozhang带你学SpringCloud》</a>
          </div>
          <div>微服务监控平台： Spring Cloud Admin。</div>
          <div>微服务链路链路追踪工具：Zipkin。</div>
          <div>分布式事务框架：Seata（待集成），相关知识可以参考：<a href={'https://blog.csdn.net/huantai3334/article/details/109169182'} target={'_blank'}>《SpringCloud的异常处理体系--分布式事务Seata（三）》</a></div>
          <div>容器化部署：Docker，相关知识可以参考：<a href={'https://blog.csdn.net/huantai3334/article/details/98473442'} target={'_blank'}>《hanxiaozhang带你学Docker》</a></div>
          <div>视图层框架：SpringMVC。</div>
          <div>持久层框架：MyBatis。</div>
          <div>安全框架：Oauth2.0 + Spring Security。</div>
          <div>缓存框架：Redis。</div>
          <div>数据库连接池：Alibaba Druid。</div>
          <div>工具类：Lombok、Hutool等。</div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            前端(TypeScript、ES6)：
          </h4>
          <div>
            前端框架：React。
          </div>
          <div>
            前端模板：Ant Design、Ant Design Pro。
          </div>
          <div>
            细化框架：Umi、Umi-Request、Dva、Mock、React Hooks、Webpack、Less等。
          </div>
          <div>

            前端容器：Nginx。
          </div>
        </div>

        <div style={{marginBottom: '0.5em'}}>
          <h4>
            <div className={styles['status-point']}></div>
            数据库：
          </h4>
          MySQL、Redis。
        </div>

        <div>
          <h4>
            <div className={styles['status-point']}></div>
            项目管理：
          </h4>
          <div>
            版本管理：Git。
          </div>
          <div>
            项目构建：Maven。
          </div>
        </div>

      </div>
  },
  {
    id: '4',
    title: '开发工具：',
    tags: [
      {
        colour: 'magenta',
        label: 'Idea'
      },
      {
        label: 'WebStorm'
      },
      {
        colour: 'green',
        label: 'Navicat'
      },
      {
        colour: 'orange',
        label: 'RedisDesktopManager'
      },],
  },

];


/**
 * 处理标签
 *
 * @param tags
 */
const handleTag = (tags: TagDataType[] | undefined) => {
  return (
    tags !== undefined &&
    <span>
  {tags.map((item, index) => {
    return <Tag color={item.colour ? item.colour : 'blue'} key={index}>{item.label}</Tag>;
  })}
  </span>);
}

const HomePage: React.FC<{}> = () => {

  return (
    <Row gutter={24}>
      <Col xl={24} lg={24} md={24} sm={24} xs={24} style={{marginBottom: 24}}>
        <Card
          title={"hanxiaozhang 管理系统"}
          bordered={false}
        >
          <List<ListItemDataType>
            size="large"
            className={styles.articleList}
            rowKey="id"
            itemLayout="vertical"
            dataSource={introduceList}
            renderItem={({contents, id, tags, title}) => (
              <List.Item key={id}>
                <List.Item.Meta
                  title={title}
                  description={
                    handleTag(tags)
                  }
                />
                <div style={styles.statusPoint}></div>
                <div className={styles.listContent}>
                  <div className={styles.description}>{contents}</div>
                </div>

              </List.Item>
            )}
          />
        </Card>
      </Col>
    </Row>

  )

}

export default HomePage;
