

export interface TagDataType {
  colour?: string;
  label: string;
}

export interface ListItemDataType {
  id: string;
  title: string;
  tags?: TagDataType[];
  contents?: React.ReactNode;
}
