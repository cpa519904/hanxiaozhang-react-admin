import request from '@/utils/request';
import {TableListParams} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listLog, {
    params,
  });
}


/**
 * 删除
 *
 * @param logId
 */
export async function remove(logId: number) {
  return request(api.removeLog, {
    method: 'POST',
    data: {
      'logId': logId,
    },
    requestType: 'form',
  });
}


/**
 * 批量删除
 *
 * @param logIds
 */
export async function batchRemove(logIds: number[]) {
  return request(api.batchRemoveLog, {
    method: 'POST',
    data: {
      'logIds': logIds,
    },
    requestType: 'form',
  });
}

