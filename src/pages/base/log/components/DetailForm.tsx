import React, {useEffect} from 'react';
import {Form, Input, Modal, Row, Col, DatePicker} from 'antd';
import moment from 'moment';
import stringUtil from '@/utils/stringUtil'


export interface UpdateFormProps {
  detailModalVisible: boolean;
  handleDetailModalVisible: any;
  detailFormValues: any;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {detailModalVisible, handleDetailModalVisible, detailFormValues, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    if (detailFormValues !== undefined && typeof detailFormValues.createDate === "string" && stringUtil.isNotEmpty(detailFormValues.createDate)) {
      detailFormValues.createDate = moment(detailFormValues.createDate.replace("T", " "), 'YYYY-MM-DD HH:mm:ss')
    }
    form.setFieldsValue({...detailFormValues});
  }, [detailFormValues, detailModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleDetailModalVisible(false);
    if (actionRef.current) {
      actionRef.current.reload();
    }
  };


  return (
    <Modal
      forceRender
      title="详情日志"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={detailModalVisible}
      onCancel={onCancel}
      footer={null}
    >

      <Form className="detailForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='用户名'
              name="username"
            >
              <Input placeholder="请输入用户名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='用户操作'
              name="operation"
            >
              <Input placeholder="请输入用户操作"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='模块名称'
              name="moduleName"
            >
              <Input placeholder="请输入模块名称"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='类型'
              name="type"
            >
              <Input placeholder="请输入类型"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='Ip'
              name="ip"
            >
              <Input placeholder="请输入Ip"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='响应时间'
              name="time"
            >
              <Input placeholder="请输入响应时间"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='创建时间'
              name="createDate"
            >
              <DatePicker format="YYYY-MM-DD HH:mm:ss"/>
            </Form.Item>
          </Col>
        </Row>


        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='请求方法'
              name="method"
            >
              <Input placeholder="请输入请求方法"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='请求参数'
              name="params"
            >
              <Input.TextArea rows={4} placeholder="请求参数"/>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default UpdateForm;
