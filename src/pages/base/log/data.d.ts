export interface TableListItem {

  logId: number; // 日志主键
  userId: number; // 用户id
  username: string; // 用户名
  moduleName: string; // 模块名称
  operation: string; // 用户操作
  time: number; // 响应时间
  method: string; // 请求方法
  params: string; // 请求参数
  ip: string; // IP地址
  type: number; // 类型
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
