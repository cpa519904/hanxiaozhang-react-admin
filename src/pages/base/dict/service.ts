import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listDict, {
    params,
  });
}

/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateDict, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param dictId
 * @param type
 */
export async function remove(dictId: number, type: string) {
  return request(api.removeDict, {
    method: 'POST',
    data: {
      'dictId': dictId,
      'type': type,
    },
    requestType: 'form',
  });
}


/**
 * 添加
 *
 * @param params
 */
export async function save(params: TableListItem) {
  return request(api.saveDict, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}

/**
 * 批量删除
 *
 * @param dictIds
 * @param types
 */
export async function batchRemove(dictIds: number[], types: string[]) {
  return request(api.batchRemoveDict, {
    method: 'POST',
    data: {
      'dictIds': dictIds,
      'types': types,
    },
    requestType: 'form',
  });
}

