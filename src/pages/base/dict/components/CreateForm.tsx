import React from 'react';
import {Modal, Form, Row, Col, Input, InputNumber, Radio} from 'antd';
import {TableListItem} from "../data.d";

interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  handleSave: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible,handleSave, actionRef} = props;

  const [form] = Form.useForm();


  const onCancel = () => {
    form.resetFields();
    handleModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      // console.log(values)
      const success = await handleSave(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      destroyOnClose
      title="新建字典"
      visible={modalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >
      <Form className="addForm"
            initialValues={{status: 1}}
            form={form}
            layout="vertical"
      >

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='标签名'
              name="name"
              rules={[{required: true, message: '请输入标签名'}]}
            >
              <Input placeholder="请输入标签名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='标签值'
              name="value"
              rules={[{required: true, message: '请输入标签值'}]}
            >
              <Input placeholder="请输入标签值"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='类型'
              name="type"
              rules={[{required: true, message: '请输入类型'}]}
            >
              <Input placeholder="请输入类型"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='类型名称'
              name="typeName"
              rules={[{required: true, message: '请输入类型名称'}]}
            >
              <Input placeholder="请输入类型名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='字典排序'
              name="dictOrder"
              rules={[{required: true, message: '请输入字典排序'}]}
            >
              <InputNumber/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='描述'
              name="description"
            >
              <Input.TextArea rows={2} placeholder="请输入描述"/>
            </Form.Item>
          </Col>
        </Row>

      </Form>

    </Modal>
  );
};

export default CreateForm;
