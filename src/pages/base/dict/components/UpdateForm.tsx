import React, {useEffect} from 'react';
import {Form, Input, Modal, Radio, Row, Col, InputNumber} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.dictId = updateFormValues.dictId;
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改字典"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='标签名'
              name="name"
              rules={[{required: true, message: '请输入标签名'}]}
            >
              <Input placeholder="请输入标签名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='标签值'
              name="value"
              rules={[{required: true, message: '请输入标签值'}]}
            >
              <Input placeholder="请输入标签值"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='类型'
              name="type"
              rules={[{required: true, message: '请输入类型'}]}
            >
              <Input placeholder="请输入类型"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='类型名称'
              name="typeName"
              rules={[{required: true, message: '请输入类型名称'}]}
            >
              <Input placeholder="请输入类型名称"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='字典排序'
              name="dictOrder"
              rules={[{required: true, message: '请输入字典排序'}]}
            >
              <InputNumber/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='状态'
              name="status"
              rules={[{required: true, message: '请选择状态'}]}
            >
              <Radio.Group>
                <Radio value={1}>启用</Radio>
                <Radio value={0}>禁用</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='描述'
              name="description"
            >
              <Input.TextArea rows={2} placeholder="请输入描述"/>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default UpdateForm;
