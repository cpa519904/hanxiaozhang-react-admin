export interface TableListItem {

  dictId: number; // 字典主键
  parentId: number; // 父id
  name: string; //  标签名
  value: string; // 标签值
  type: string; // 类型
  typeName: string; // 类型名称
  description: string; // 描述
  dictOrder: number; // 排序
  status: number; // 状态 0:禁用，1:正常
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
