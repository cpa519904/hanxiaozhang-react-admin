import {Component} from 'react';
import FileViewer from 'react-file-viewer';
import PDF from 'react-pdf-js';

/**
 * 预览组件
 *
 */
export class Preview extends Component {


  constructor(props) {
    super(props);
    this.state = {
      fileType: props.fileType,
      filePath: props.filePath,
    };
  }


  render() {
    const {fileType, filePath} = this.state;
    // console.log(this.state)
    return (
      filePath==='pdf'?
        <PDF
          file={filePath}
        />
      :<FileViewer fileType={fileType}
                          filePath={filePath}
                          errorComponent={this.onErrorComponent()} // [可选]：发生错误时呈现的组件，而不是react-file-viewer随附的默认错误组件。
                          unsupportedComponent={this.unsupportedComponent()} // [可选]：在不支持文件格式的情况下呈现的组件。
                          onError={(e) => {
                            console.log(e)
                          }}
      />

    );

  }

  onErrorComponent() {
    console.log("预览出错!");
  }

  unsupportedComponent() {
    console.log("不支持文件格式预览!");
  }

}
