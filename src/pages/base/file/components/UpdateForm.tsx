import React, {useEffect} from 'react';
import {Form, Input, Modal, Row, Col} from 'antd';
import {TableListItem} from "../data.d";


export interface UpdateFormProps {
  updateModalVisible: boolean;
  handleUpdateModalVisible: any;
  updateFormValues: any;
  handleUpdate: (fields: TableListItem) => Promise<boolean>;
  actionRef: any;
}


const UpdateForm: React.FC<UpdateFormProps> = (props) => {

  const {updateModalVisible, handleUpdateModalVisible, updateFormValues, handleUpdate, actionRef} = props;
  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({...updateFormValues});
  }, [updateFormValues, updateModalVisible]);

  const onCancel = () => {
    form.resetFields();
    handleUpdateModalVisible(false);
  };

  const onOk = () => {
    form.validateFields().then(async (values: TableListItem) => {
      values.fileId = updateFormValues.fileId;
      const success = await handleUpdate(values);
      if (success) {
        if (actionRef.current) {
          actionRef.current.reload();
        }
        onCancel();
      }
    });
  };

  return (
    <Modal
      forceRender
      title="修改文件"
      bodyStyle={{padding: '32px 40px 48px'}}
      destroyOnClose
      visible={updateModalVisible}
      onCancel={onCancel}
      onOk={onOk}
      okText="确定"
      cancelText="取消"
    >

      <Form className="updateForm" form={form}
            layout="vertical"
      >
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label='文件名'
              name="fileName"
              rules={[{required: true, message: '请输入文件名'}]}
            >
              <Input placeholder="请输入文件名"/>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label='文件全名'
              name="fileFull"
              rules={[{required: true, message: '请输入文件全名'}]}
            >
              <Input placeholder="请输入文件全名"/>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={24}>
            <Form.Item
              label='URL地址'
              name="url"
              rules={[{required: true, message: '请输入URL地址'}]}
            >
              <Input placeholder="请输入URL地址"/>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </Modal>
  );
};

export default UpdateForm;
