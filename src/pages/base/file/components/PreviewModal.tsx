import React from 'react';
import {Drawer} from 'antd';
import {Preview} from './Preview.js';

interface CreateFormProps {
  modalVisible: boolean;
  handleModalVisible: any;
  updateFormValues: any;
  actionRef: any;
}


const CreateForm: React.FC<CreateFormProps> = (props) => {

  const {modalVisible, handleModalVisible,updateFormValues, actionRef} = props;



  const onClose = () => {
    if (actionRef.current) {
      actionRef.current.reload();
    }
    handleModalVisible(false);
  };

  return (

    <Drawer
      title="预览"
      placement="right"
      width={800}
      onClose={onClose}
      visible={modalVisible}
    >
      {modalVisible && <Preview fileType={updateFormValues.fileExt} filePath={updateFormValues.url}/>}

    </Drawer>
  );
};

export default CreateForm;
