import request from '@/utils/request';
import {TableListParams, TableListItem} from './data.d';
import api from "@/utils/api";

/**
 * 列表
 *
 * @param params
 */
export async function list(params?: TableListParams): Promise<any> {
  return request(api.listFile, {
    params,
  });
}

/**
 * 更新
 *
 * @param params
 */
export async function update(params: TableListItem) {
  return request(api.updateFile, {
    method: 'POST',
    data: {
      ...params
    },
    requestType: 'form',
  });
}


/**
 * 删除
 *
 * @param fileId
 */
export async function remove(fileId: number) {
  return request(api.removeFile, {
    method: 'POST',
    data: {
      'fileId': fileId,
    },
    requestType: 'form',
  });
}


/**
 * 批量删除
 *
 * @param fileIds
 */
export async function batchRemove(fileIds: number[]) {
  return request(api.batchRemoveFile, {
    method: 'POST',
    data: {
      'fileIds': fileIds,
    },
    requestType: 'form',
  });
}




