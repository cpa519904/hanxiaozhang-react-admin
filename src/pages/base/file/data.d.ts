export interface TableListItem {
  fileId: number; // 主键
  fileName: string; // 文件名
  fileExt: string; // 文件扩展名
  fileFull: string; // 文件全名
  filePath: string; // 文件路径
  url: string; // URL地址
  serverPath: string; // 文件服务器地址
  groupPath: string; // 文件服务器路
  belongId: number; // 文件属于
  type: string; // 文件附属类型
  fileTag: string; // 文件附属标签
  typeName: string; // 文件类型名称
  fileTagName: string; // 文件标签
  uuid: string; // uuid
  createBy: string; // 创建者
  createDate: Date; // 创建时间
  updateBy: string; // 更新者
  updateDate: Date; // 更新时间
  remarks: string;  // 备注信息
}

export interface TableListPagination {
  total: number;
  pageSize: number;
  current: number;
}

export interface TableListData {
  list: TableListItem[];
  pagination: Partial<TableListPagination>;
}

export interface TableListParams {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: { [key: string]: any[] };
  sorter?: { [key: string]: any };
}
