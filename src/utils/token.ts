import {Storage} from './stroage';

// OAuth2.0返回token的模式
// {
//     "access_token": "2b2b7ea3-abbc-4d64-a5c1-b23a7fb1cebc",
//     "token_type": "bearer",
//     "refresh_token": "03bb2785-3913-4cb2-bd26-e84050f61be2",
//     "expires_in": 20664,
//     "scope": "hanxiaozhang"
//   }

/**
 * 设置token信息
 *
 * @param props
 */
export function setTokens(props: any) {
  const {access_token, expires_in, refresh_token} = props;
  Storage.setAge(expires_in * 1000).set('access_token', `bearer ${access_token}`)
  Storage.setAge(30 * 24 * 60 * 60 * 1000).set('refresh_token', refresh_token)
}

/**
 * 清除Token
 */
export function clearTokens() {
  Storage.remove('access_token')
  return Storage.remove('refresh_token');
}

/**
 * 获取access_token
 */
export function getAccessToken() {
  return Storage.get('access_token');
}

/**
 * 判断access_token是否过期
 */
export function isExpireAccessToken(){
  return Storage.isExpire('access_token');
}

/**
 * 获取refresh_token
 */
export function getRefreshToken() {
  return Storage.get('refresh_token');
}

/**
 * 判断refresh_token是否过期
 */
export function isExpireRefreshToken(){
  return Storage.isExpire('refresh_token');
}
