/**
 * 字符串工具类
 *
 */
class stringUtil {

  /**
   * 判断字符串是否为空或undefined,不判断为0,不判断为false
   * @param str
   * @returns {boolean}
   */
  public static isEmpty = (str: any): boolean => {
    if (str === null || str === '' ||
      str === undefined || str.length === 0
    ) {
      return true
    }
    return false

  };

  /**
   * 判断字符串是否不为空或不undefined,不判断为0,不判断为false
   * @param str
   * @returns {boolean}
   */
  public static isNotEmpty = (str: any): boolean => {
    if (
      str === null || str === '' ||
      str === undefined || str.length === 0
    ) {
      return false
    }
    return true

  };
}

export default stringUtil
