import React, { Context, createContext, useContext } from 'react'
import filterPermissionKey from './utils/filter'
import hasPermissionKey from './utils/vlidate'

/**
 *  AuthConsumer实体
 */
interface AuthConsumerProps {
  name: string;
  children: React.ReactNode
}

const AuthContext: Context<any> = createContext({
  routeProps: [],
})

export const AuthConsumer = (props: AuthConsumerProps) => {
  const context = useContext(AuthContext)
  const { routeProps } = context
  // console.log(context)
  const permissionKeys = filterPermissionKey(routeProps).permissionKey
  // console.log(permissionKeys)
  const result = hasPermissionKey(permissionKeys, props.name)
  return result ? <span>{props.children}</span> : <></>
}

export const AuthProvider = AuthContext.Provider

