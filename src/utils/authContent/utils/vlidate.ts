export default function validate (permissionKeys: string[] = [], name: string) {
  return permissionKeys.indexOf(name) > -1
}
