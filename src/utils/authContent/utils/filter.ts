
/**
 * 路由实体
 */
interface RouteProps {
  path: string,
  name: string,
  icon?: string,
  component?: string,
  authority?: string[],
  permission?: PermissionProps[],
  routes?: RouteProps[],
}

/**
 * 权限实体
 */
interface PermissionProps {
  name: string,
  key: string,
}

/**
 * 递归获取permissionKey
 * @param props
 * @param permissionKey
 */
const recursionPermissionKey = (props: RouteProps[],permissionKey: string[])=>{
  props.forEach((prop) => {
    if(prop.permission){
      prop.permission.forEach((item)=>{
        permissionKey.push(item.key)
      })
    }
    if (prop.routes) {
      recursionPermissionKey(prop.routes, permissionKey)
    }
  })
}

/**
 * 过滤key
 *
 * @param props
 */
export default function filter(props: RouteProps[]) {
  const permissionKey: string[] = []
  recursionPermissionKey(props,permissionKey)
  return { permissionKey }
}

