const api = {
  // 环境url
  dev: 'http://localhost',
  pro: 'http://localhost:80',
  // 登录登出相关
  accountLogin: '/api/system/oauth/token', // 账号登录
  listRoute: '/api/system/router', // 获取路由
  captcha: '/api/system/captcha', // 获取验证码
  logout: '/api/system/token/logout', // 登出
  // 部门
  listDept: '/api/system/dept/list', // 列表
  saveDept: '/zuul/api/system/dept/save', // 添加
  updateDept: '/zuul/api/system/dept/update', // 更新
  removeDept: '/zuul/api/system/dept/remove', // 删除
  batchRemoveDept: '/zuul/api/system/dept/batchRemove', // 批量删除
  deptTree: '/api/system/dept/tree', // 部门树
  // 岗位
  listJob: '/api/system/job/list', // 列表
  saveJob: '/zuul/api/system/job/save', // 添加
  updateJob: '/zuul/api/system/job/update', // 更新
  removeJob: '/zuul/api/system/job/remove', // 删除
  batchRemoveJob: '/zuul/api/system/job/batchRemove', // 批量删除
  // 菜单
  listMenu: '/api/system/menu/list', // 列表
  saveMenu: '/zuul/api/system/menu/save', // 添加
  updateMenu: '/zuul/api/system/menu/update', // 更新
  removeMenu: '/zuul/api/system/menu/remove', // 删除
  batchRemoveMenu: '/zuul/api/system/menu/batchRemove', // 批量删除
  menuTree: '/api/system/menu/tree', // 菜单树
  // 角色
  listRole: '/api/system/role/list', // 列表
  saveRole: '/zuul/api/system/role/save', // 添加
  updateRole: '/zuul/api/system/role/update', // 更新
  removeRole: '/zuul/api/system/role/remove', // 删除
  batchRemoveRole: '/zuul/api/system/role/batchRemove', // 批量删除
  // 用户
  listUser: '/api/system/user/list', // 列表
  saveUser: '/zuul/api/system/user/save', // 添加
  updateUser: '/zuul/api/system/user/update', // 更新
  removeUser: '/zuul/api/system/user/remove', // 删除
  batchRemoveUser: '/zuul/api/system/user/batchRemove', // 批量删除
  resetPassword: '/api/system/user/resetPassword', // 重置密码
  userUpdatePassword: '/api/system/user/updatePassword', // 用户更新密码
  userUpdateUser: '/zuul/api/system/user/updateUser', // 用户更新信息
  currentUser: '/api/system/currentUser', // 获取当前用户
  // 数据字典
  listDict: '/api/base/dict/list', // 列表
  saveDict: '/zuul/api/base/dict/save', // 添加
  updateDict: '/zuul/api/base/dict/update', // 更新
  removeDict: '/zuul/api/base/dict/remove', // 删除
  batchRemoveDict: '/zuul/api/base/dict/batchRemove', // 批量删除
  // 日志
  listLog: '/api/base/log/list', // 列表
  removeLog: '/zuul/api/base/log/remove', // 删除
  batchRemoveLog: '/zuul/api/base/log/batchRemove', // 批量删除
  // 生成代码
  listGenerator: '/api/base/generator/list', // 列表
  updateGenerator: '/api/base/generator/update', // 更新配置
  codeGenerator: '/zuul/api/base/generator/download/code', // 生成
  batchCodeGenerator: '/zuul/api/base/generator/download/batchCode', // 批量生成
  getConfigGenerator: '/api/base/generator/getConfig', // 获取配置
  // 定时任务
  listJobTask: '/api/base/jobTask/list', // 列表
  saveJobTask: '/zuul/api/base/jobTask/save', // 添加
  updateJobTask: '/zuul/api/base/jobTask/update', // 更新
  removeJobTask: '/zuul/api/base/jobTask/remove', // 删除
  batchRemoveJobTask: '/zuul/api/base/jobTask/batchRemove', // 批量删除
  updateStatusJobTask: '/zuul/api/base/jobTask/updateStatus', // 更新状态
  // 文件管理
  listFile: '/api/base/file/list', // 列表
  updateFile: '/zuul/api/base/file/update', // 更新
  removeFile: '/zuul/api/base/file/remove', // 删除
  batchRemoveFile: '/zuul/api/base/file/batchRemove', // 批量删除
  uploadFile: '/zuul/api/base/file/uploadFile', // 上传文件

};

export default api;
