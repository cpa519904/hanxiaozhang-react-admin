/**
 * 文件下载
 *
 * @param response
 * @param fileName
 * @param mimeEnum
 */
export function downLoadFile(response: any, fileName: string, mimeEnum: MimeEnum) {
  // console.log(fileName);
  const blob = new Blob([response], {type: mimeEnum});
  const a = document.createElement('a');
  a.download = fileName;
  a.href = window.URL.createObjectURL(blob);
  a.click();
};


/**
 *
 * Mime枚举
 */
export enum MimeEnum {
  ZIP = 'application/zip',
  JPEG = "image/jpeg",
  PNG = "image/png",
  PDF = 'application/zip',
  DOC = "application/msword",
  DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  XLS = "application/vnd.ms-excel",
  XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  PPT = "application/vnd.ms-powerpoint",
  PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
}
