// eslint-disable-next-line import/no-extraneous-dependencies
import {Request, Response} from 'express';


const response = {
  "code": 1,
  "msg": "成功",
  "time": 1616826747620,
  "data": ""
}


function list(req: Request, res: Response, u: string, b: Request) {
  // console.log(1)
  // console.log(Response)
  // console.log(u)
  // console.log(b)
  const a = [
    {

    }
  ]

  const result = {
    "code": 1,
    "msg": "成功",
    "time": 1616826747620,
    "data": {
      data: a,
      total: 3,
      success: true,
    }
  };

  return res.json(result);
}


function save(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function update(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function remove(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function batchRemove(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

export default {
  'GET /api/base/file/list': list,
  'POST /zuul/api/base/file/save': save,
  'POST /zuul/api/base/file/update': update,
  'POST /zuul/api/base/file/remove': remove,
  'POST /zuul/api/base/file/batchRemove': batchRemove,
}
