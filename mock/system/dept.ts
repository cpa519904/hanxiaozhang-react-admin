import {Request, Response} from 'express';


const response = {
  "code": 1,
  "msg": "成功",
  "time": 1616826747620,
  "data": ""
}


function list(req: Request, res: Response, u: string, b: Request) {
  // console.log(1)
  // console.log(Response)
  // console.log(u)
  // console.log(b)
  const a = [
    {
      "createBy": null,
      "createDate": null,
      "updateBy": null,
      "updateDate": "2021-03-21 12:44:36",
      "remarks": null,
      "delFlag": 0,
      "deptId": 1,
      "parentId": 0,
      "deptName": "顶级部门",
      "deptOrder": 1,
      "subCount": 3,
      "status": 1,
      "parentName": "无",
      "children": [
        {
          "createBy": null,
          "createDate": null,
          "updateBy": null,
          "updateDate": "2021-03-21 12:45:36",
          "remarks": null,
          "delFlag": 0,
          "deptId": 2,
          "parentId": 1,
          "deptName": "一级部门A",
          "deptOrder": 1,
          "subCount": 2,
          "status": 1,
          "parentName": "顶级部门",
          "children": [
            {
              "createBy": null,
              "createDate": null,
              "updateBy": null,
              "updateDate": "2021-03-21 12:46:10",
              "remarks": null,
              "delFlag": 0,
              "deptId": 5,
              "parentId": 2,
              "deptName": "一级部门A-1",
              "deptOrder": 1,
              "subCount": 1,
              "status": 1,
              "parentName": "一级部门A",
              "children": [
                {
                  "createBy": null,
                  "createDate": null,
                  "updateBy": null,
                  "updateDate": "2021-03-21 12:47:12",
                  "remarks": null,
                  "delFlag": 0,
                  "deptId": 7,
                  "parentId": 5,
                  "deptName": "一级部门A-1-1",
                  "deptOrder": 1,
                  "subCount": 0,
                  "status": 1,
                  "parentName": "一级部门A-1",
                  "children": []
                }
              ]
            },
            {
              "createBy": null,
              "createDate": null,
              "updateBy": null,
              "updateDate": "2021-03-21 12:47:06",
              "remarks": null,
              "delFlag": 0,
              "deptId": 6,
              "parentId": 2,
              "deptName": "一级部门A-2",
              "deptOrder": 2,
              "subCount": 0,
              "status": 1,
              "parentName": "一级部门A",
              "children": []
            }
          ]
        },
        {
          "createBy": null,
          "createDate": null,
          "updateBy": null,
          "updateDate": "2021-03-21 12:45:21",
          "remarks": null,
          "delFlag": 0,
          "deptId": 3,
          "parentId": 1,
          "deptName": "一级部门B",
          "deptOrder": 2,
          "subCount": 0,
          "status": 1,
          "parentName": "顶级部门",
          "children": []
        },
        {
          "createBy": null,
          "createDate": null,
          "updateBy": null,
          "updateDate": "2021-03-21 12:46:07",
          "remarks": null,
          "delFlag": 0,
          "deptId": 4,
          "parentId": 1,
          "deptName": "一级部门C",
          "deptOrder": 3,
          "subCount": 0,
          "status": 1,
          "parentName": "顶级部门",
          "children": []
        }
      ]
    }
  ]

  const result = {
    "code": 1,
    "msg": "成功",
    "time": 1616826747620,
    "data": {
      data: a,
      total: 3,
      success: true,
    }
  };

  return res.json(result);
}


function save(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function update(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function remove(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

function batchRemove(req: Request, res: Response, u: string, b: Request) {
  return res.json(response)
}

export default {
  'GET /api/system/dept/list': list,
  'POST /zuul/api/system/dept/save': save,
  'POST /zuul/api/system/dept/update': update,
  'POST /zuul/api/system/dept/remove': remove,
  'POST /zuul/api/system/dept/batchRemove': batchRemove,
}
