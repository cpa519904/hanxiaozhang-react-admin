import {Request, Response} from "express";

// todo 有这种跳转就不行
// {
//   path: '/list/search',
//   redirect: '/list/search/articles',
// },
export default {
  'GET /api/system/router1': {
    "code": 1,
    "msg": "成功",
    "time": 1616826747620,
    "data": [
      {
        name: 'base',
        path: '/base',
        icon: 'tool', // ToolOutlined
        routes: [
          {
            name: 'dict',
            icon: 'tool',
            path: '/base/dict',
            component: '/base/dict',
            permission: [
              {
                name: "添加",
                key: "base:dict:save"
              },
              {
                name: "修改",
                key: "base:dict:update"
              },
              {
                name: "删除",
                key: "base:dict:remove"
              },
              {
                name: "批量删除",
                key: "base:dict:batchRemove"
              },
            ],
          },
          {
            name: 'log',
            icon: 'tool',
            path: '/base/log',
            component: '/base/log',
            permission: [
              {
                name: "删除",
                key: "base:log:remove"
              },
              {
                name: "批量删除",
                key: "base:log:batchRemove"
              },
            ],
          },
        ],
      },
      {
        path: '/system',
        name: 'system',
        icon: 'setting',
        routes: [
          {
            name: 'dept',
            icon: 'setting',
            path: '/system/dept',
            component: '/system/dept',
            permission: [
              {
                name: "添加",
                key: "system:dept:save"
              },
              {
                name: "修改",
                key: "system:dept:update"
              },
              {
                name: "删除",
                key: "system:dept:remove"
              },
              {
                name: "批量删除",
                key: "system:dept:batchRemove"
              },
            ]
          },
          {
            name: 'job',
            icon: 'setting',
            path: '/system/job',
            component: '/system/job',
            permission: [
              {
                name: "添加",
                key: "system:job:save"
              },
              {
                name: "修改",
                key: "system:job:update"
              },
              {
                name: "删除",
                key: "system:job:remove"
              },
              {
                name: "批量删除",
                key: "system:job:batchRemove"
              },
            ]
          },
          {
            name: 'menu',
            icon: 'setting',
            path: '/system/menu',
            component: '/system/menu',
            permission: [
              {
                name: "添加",
                key: "system:menu:save"
              },
              {
                name: "修改",
                key: "system:menu:update"
              },
              {
                name: "删除",
                key: "system:menu:remove"
              },
              {
                name: "批量删除",
                key: "system:menu:batchRemove"
              },
            ]
          },
          {
            name: 'role',
            icon: 'setting',
            path: '/system/role',
            component: '/system/role',
            permission: [
              {
                name: "添加",
                key: "system:role:save"
              },
              {
                name: "修改",
                key: "system:role:update"
              },
              {
                name: "删除",
                key: "system:role:remove"
              },
              {
                name: "批量删除",
                key: "system:role:batchRemove"
              },
            ]
          },
          {
            name: 'user',
            icon: 'setting',
            path: '/system/user',
            component: '/system/user',
            permission: [
              {
                name: "添加",
                key: "system:user:save"
              },
              {
                name: "修改",
                key: "system:user:update"
              },
              {
                name: "删除",
                key: "system:user:remove"
              },
              {
                name: "批量删除",
                key: "system:user:batchRemove"
              },
              {
                name: "重置密码",
                key: "system:user:resetPassword"
              },
            ]
          },
        ],
      },
      {
        name: 'account',
        icon: 'user',
        path: '/account',
        routes: [
          {
            name: 'settings',
            icon: 'smile',
            path: '/account/settings',
            component: '/account/settings',
          },
        ],
      },
    ]
  },
  'GET /api/system/router': {
    "code": 1,
    "msg": "成功",
    "time": 1616826747620,
    "data": []
  },
  'POST /api/system/router1': (req: Request, res: Response) => {
    res.status(401).send({
      status: 401,
      error: 'error',
      message: 'error',
    });
  },
};
