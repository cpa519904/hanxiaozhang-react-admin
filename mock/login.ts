import { Request, Response } from 'express';

const waitTime = (time: number = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

export default {
  'POST /api/system/oauth/token': async (req: Request, res: Response) => {
    // console.log(req.query)
    // url上参数 req.query ，body中参数req.body
    const { password, username, type } = req.query;
    await waitTime(2000);
    if (password === '123456' && username === 'admin') {
      res.send({
            "access_token": "546553e3-b103-4c67-bf8d-15153ff6d371",
            "token_type": "bearer",
            "refresh_token": "624be00a-5634-4a60-b806-b509da46fdff",
            "expires_in": 20664,
            "scope": "hanxiaozhang"
      });
      return;
    }
    if (type === 'mobile') {
      res.send({
        status: 'ok',
        type,
        currentAuthority: 'admin',
      });
      return;
    }

    res.send({
      status: 'error',
      type,
      currentAuthority: 'guest',
    });
  },


  'POST /api/register': (req: Request, res: Response) => {
    res.send({ status: 'ok', currentAuthority: 'user' });
  },

};
