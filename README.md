## 环境准备

Install `node_modules`:

```bash
npm install --registry=https://registry.npm.taobao.org
```

or

```bash
yarn
```

### 启动项目

```bash
npm start
```

### 打包项目

```bash
npm run build
```

### 检查代码

```bash
npm run lint
```

也可以使用脚本自动修复某些 lint 错误：

```bash
npm run lint:fix
```

### 测试代码

```bash
npm test
```

## 遇到问题：

### 1.<SelectLang /> 插件下拉如何只显示 中文和英文两种

解决，原因：有部分模块的locales没有删除感觉其他语言配置

### 2.关闭 mock: 
+ I.当本地开发完毕之后，如果服务器的接口满足之前的约定，那么只需要关闭 mock 数据或者代理到服务端的真实接口地址即可。

```
npm run start:no-mock
```
+ II. 在 config\config.ts 配置(注意：mock/user.ts没有根路径)

```
mock: {
     exclude: ['mock/user.ts']
 },
```

### 3. 开启默认进入登录页面src\utils\authority.ts进行修改,并且配置config\config.ts中的路由。
这么做没什么卵用

### 4. 把config/config.tx中路由拆出来配置方法(没有用到)：

```
来自umi.js官网
// config/routes.ts

export default [
    { exact: true, path: '/', component: 'index' },
];

// config/config.ts

import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
});
```



# 问题Bug：
## 1. 消息提醒下拉菜单，第一次点击有报错，看了一个多小时间没有解决。
```
devScripts.js:5836 Warning: Received `false` for a non-boolean attribute `selectable`.
If you want to write it to the DOM, pass a string instead: selectable="false" or selectable={value.toString()}.
If you used to conditionally omit it with selectable={condition && value}, pass selectable={condition ? value : undefined} instead.

devScripts.js:5836 Warning: React does not recognize the `expandIcon` prop on a DOM element. 
If you intentionally want it to appear in the DOM as a custom attribute, spell it as lowercase `expandicon` instead.
 If you accidentally passed it from a parent component, remove it from the DOM element.
```
## 2.角色模块添加，修改菜单树渲染慢，跟自己写的方式有问题，需要改正一下。

## 3.不能覆盖ant design 样式

## 4.用户管理 选择部门样式问题，目前还不会解决

## 5.登录后，调用当前用是，还没有保存上token出现401问题,正常情况下不会出现。



# 开发计划
## 一、系统管理:
### 一期计划：
部门、角色、菜单、岗位、用户管理 （完成20210403）
### 二期计划(基础管理)：
数据字典管理 （完成20210408）
### 三期计划：
数据权限的开发、日志管理（完成202104010）、定时任务管理
## 二、登录管理：
### 一期计划：
登录、登出（前端完成，使用mock，未对接后端）（完成）
### 二期计划：
增加验证码（后端放在Redis）（完成20210404）
## 三、权限管理：
### 一期计划：
后端动态加载菜单、按钮级别权限（前端完成，使用mock，未对接后端）（完成）
## 四、用户中心：
### 一期计划：
用户中心 （完成20210407）
### 二期计划：
用户自己重置密码
## 五、首页：
### 一期计划：
首页
## 六、消息：
### 一期计划：
导航栏消息报错问题
## 七、工具管理：
### 一期计划：
生成代码

